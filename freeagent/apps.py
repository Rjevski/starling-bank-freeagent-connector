from django.apps import AppConfig


class FreeagentConfig(AppConfig):
    name = 'freeagent'
    verbose_name = 'FreeAgent'
