from django.contrib import admin

from freeagent.models import BankAccount, Instance
from oauth.admin import BaseOAuthModelAdmin


# Register your models here.

class InstanceAdmin(BaseOAuthModelAdmin):
    exclude = ['access_token', 'refresh_token', 'expires_at', 'token_type']


class BankAccountAdmin(admin.ModelAdmin):
    list_display = ['name', 'instance']


admin.site.register(Instance, InstanceAdmin)
admin.site.register(BankAccount, BankAccountAdmin)
