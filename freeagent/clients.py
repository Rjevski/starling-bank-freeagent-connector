from json import JSONDecodeError

import requests
from django.conf import settings
from popget import APIClient, BodyType, GetEndpoint, PostEndpoint
from popget.client import get_response_body
from requests import Timeout


class FreeAgentClient(APIClient):
    class Config:
        base_url = settings.FREEAGENT_BASE_URL

    upload_statement = PostEndpoint(
        '/bank_transactions/statement?bank_account={account_uri}',
        body_required=True,
        body_type=BodyType.FORM_ENCODED
    )

    get_company = GetEndpoint(
        '/company'
    )

    @staticmethod
    def handle(call, request_url):
        try:
            res = call()
        except Timeout as e:
            # generate a proxy 504 'Gateway Timeout' response
            res = requests.Response()
            res.request = e.request
            res.url = request_url
            res.reason = str(e)
            res.status_code = 504

        res.raise_for_status()
        try:
            return get_response_body(res)
        except JSONDecodeError:
            # FreeAgent sometimes responds with an empty body but sets Content-Type to JSON anyway
            return res
