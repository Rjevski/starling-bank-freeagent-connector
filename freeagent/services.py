from freeagent.clients import FreeAgentClient
from freeagent.models import BankAccount


def upload_statement(bank_account: BankAccount, statement: str):
    return FreeAgentClient.upload_statement(
        account_uri=bank_account.uri,
        body={
            'statement': statement
        },
        _session=bank_account.instance.session
    )
