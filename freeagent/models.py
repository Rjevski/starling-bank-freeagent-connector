from urllib.parse import urljoin

from django.conf import settings
from django.db import models

from freeagent.clients import FreeAgentClient
from oauth.models import BaseOAuthModel, BaseOAuthModelManager


# Create your models here.

class InstanceManager(BaseOAuthModelManager):

    def create_from_oauth_callback(self, request):
        instance = super().create_from_oauth_callback(request)

        company = FreeAgentClient.get_company(_session=instance.session)['company']

        instance.name = company['name']

        return instance


class Instance(BaseOAuthModel):
    name = models.CharField(max_length=100)

    objects = InstanceManager()

    auth_url = urljoin(settings.FREEAGENT_BASE_URL, 'approve_app')
    refresh_url = urljoin(settings.FREEAGENT_BASE_URL, 'token_endpoint')

    client_id = settings.FREEAGENT_CLIENT_ID
    client_secret = settings.FREEAGENT_CLIENT_SECRET

    def __str__(self):
        return self.name


class BankAccount(models.Model):
    instance = models.ForeignKey(Instance, on_delete=models.CASCADE, related_name='bank_accounts')
    name = models.CharField(max_length=100)
    uri = models.TextField('URI')

    def __str__(self):
        return self.name
