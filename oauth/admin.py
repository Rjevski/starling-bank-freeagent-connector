from functools import update_wrapper

from django.conf.urls import url
from django.contrib import admin
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect


class BaseOAuthModelAdmin(admin.ModelAdmin):
    """A ModelAdmin that supports creating OAuth client models."""

    def add_view(self, request, form_url='', extra_context=None) -> HttpResponseRedirect:
        """Redirect to the OAuth authentication URI."""
        if not self.has_add_permission(request):
            raise PermissionDenied

        return redirect(self.model.objects.get_login_url(request))

    def get_urls(self) -> list:
        """Add the OAuth callback URL to the admin URLs."""

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        return super().get_urls() + [
            url(r'oauth_callback', wrap(self.process_oauth_callback),
                name=f'{self.model._meta.app_label}_{self.model._meta.model_name}_oauth_callback')
        ]

    def process_oauth_callback(self, request) -> HttpResponse:
        """Create new object based on OAuth callback."""
        if not self.has_add_permission(request):
            raise PermissionDenied

        obj = self.model.objects.create_from_oauth_callback(request)
        self.save_model(request, obj, None, False)

        change_message = [{'added': {}}]
        self.log_addition(request, obj, change_message)

        return super().response_add(request, obj)
