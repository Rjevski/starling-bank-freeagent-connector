import datetime
import logging

from freeagent.services import upload_statement
from ofx.services import render_statement
from starling.clients import StarlingClient
from starling.models import Account, WebhookEvent
from transactions.models import Link

TRANSACTION_DETAIL_GETTER_MAP = {
    'DIRECT_DEBIT': StarlingClient.get_direct_debit_transaction,
    'MASTER_CARD': StarlingClient.get_mastercard_transaction,
    'FASTER_PAYMENTS_IN': StarlingClient.get_faster_payment_inbound_transaction,
    'FASTER_PAYMENTS_OUT': StarlingClient.get_faster_payment_outbound_transaction,
}

WEBHOOK_DETAIL_GETTER_MAP = {
    'TRANSACTION_CARD': StarlingClient.get_mastercard_transaction,
    'TRANSACTION_CASH_WITHDRAWAL': StarlingClient.get_mastercard_transaction,
    'TRANSACTION_MOBILE_WALLET': StarlingClient.get_mastercard_transaction,
    'TRANSACTION_FASTER_PAYMENT_IN': StarlingClient.get_faster_payment_inbound_transaction,
    'TRANSACTION_FASTER_PAYMENT_OUT': StarlingClient.get_faster_payment_outbound_transaction,
    'TRANSACTION_DIRECT_DEBIT': StarlingClient.get_direct_debit_transaction,
}

logger = logging.getLogger(__name__)


def get_transactions(account: Account, from_: datetime.date, to: datetime.date):
    transaction_summaries = StarlingClient.get_transactions(**{
        'from': from_.strftime('%Y-%m-%d'),
        'to': to.strftime('%Y-%m-%d')
    }, _session=account.session)
    transaction_summaries = transaction_summaries['_embedded']['transactions']

    pending_transactions = []
    settled_transactions = []

    for summary in transaction_summaries:
        if summary['amount'] == 0:
            # Skip card checks & released payments
            continue

        if summary['source'] == 'MASTER_CARD':
            txn = summary_to_transaction(account, summary)

            if txn.get('status') == 'PENDING':
                pending_transactions.append(txn)
                continue
        else:
            txn = summary

        txn['settled'] = txn['created']
        settled_transactions.append(txn)

    return pending_transactions, settled_transactions


def summary_to_transaction(account: Account, summary: dict):
    getter = TRANSACTION_DETAIL_GETTER_MAP.get(summary['source'])

    if not getter:
        logger.warning('Unsupported source type, skipping', summary['source'])
        return

    return getter(transaction_id=summary['id'], _session=account.session)


def handle_new_transaction(account: Account, transaction: dict):
    if transaction['amount'] == 0:
        return

    if 'status' in transaction and transaction['status'] == 'PENDING':
        return

    links = Link.objects.filter(starling_account=account)

    for link in links:
        # TODO: figure out how to get settlement date from Starling
        transaction['settled'] = transaction['created']
        statement = render_statement(link.starling_account, [], [transaction])
        upload_statement(link.freeagent_account, statement)


def handle_webhook_event(event: WebhookEvent):
    content = event.payload['content']

    if 'transactionUid' not in content:
        logger.warning('No transaction UID associated with webhook event %s', event.pk)
        return

    getter = WEBHOOK_DETAIL_GETTER_MAP.get(content['type'], StarlingClient.get_transaction_detail)
    transaction = getter(transaction_id=content['transactionUid'], _session=event.account.session)

    return handle_new_transaction(event.account, transaction)
