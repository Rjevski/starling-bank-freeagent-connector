from django.contrib import admin

from starling.models import Account, WebhookEvent


# Register your models here.

class WebhookEventAdmin(admin.ModelAdmin):
    list_display = ['remote_id', 'account', 'created_at', 'webhook_type', 'event_class', 'message']

    def webhook_type(self, obj):
        return obj.payload.get('webhookType', 'n/a')

    def event_class(self, obj):
        try:
            return obj.payload['content']['class']
        except KeyError:
            return 'n/a'

    def message(self, obj):
        try:
            return obj.payload['content']['forCustomer']
        except KeyError:
            return 'n/a'


admin.site.register(Account)
admin.site.register(WebhookEvent, WebhookEventAdmin)
