from django.conf.urls import url

from starling.views import handle_webhook

urlpatterns = [
    url(r'handler', handle_webhook)
]
