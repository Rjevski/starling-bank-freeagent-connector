import json
from base64 import b64encode
from hashlib import sha512

import requests
from django.contrib.postgres.fields import JSONField
from django.db import models

from starling.exceptions import DuplicateWebhookException, IncorrectSignatureException


# Create your models here.

class Account(models.Model):
    name = models.CharField(max_length=100)
    access_token = models.TextField()
    webhook_secret = models.TextField()

    _session = None

    @property
    def session(self):
        if not self._session:
            self._session = requests.session()
            self._session.headers = {
                'Authorization': f'Bearer {self.access_token}'
            }
        return self._session

    def __str__(self):
        return self.name


class WebhookEventManager(models.Manager):
    def create_from_request(self, request):
        signature = request.META.get('HTTP_X_HOOK_SIGNATURE')

        for account in Account.objects.all():
            hash = sha512(account.webhook_secret.encode('ascii') + request.body).digest()
            expected_signature = b64encode(hash).decode('ascii')

            if signature != expected_signature:
                continue

            payload = json.loads(request.body)
            return self.create_from_payload(payload, account)

        raise IncorrectSignatureException

    def create_from_payload(self, payload, account):
        uid = payload['webhookNotificationUid']
        event, created = self.get_or_create(remote_id=uid, payload=payload, account=account)

        if not created:
            raise DuplicateWebhookException

        return event


class WebhookEvent(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    account = models.ForeignKey(Account, on_delete=models.DO_NOTHING, related_name='webhook_events')
    remote_id = models.TextField('Remote ID', unique=True, db_index=True)
    payload = JSONField()

    objects = WebhookEventManager()

    def __str__(self):
        return self.remote_id
