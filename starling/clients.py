from django.conf import settings
from popget import APIClient, Arg, GetEndpoint


class StarlingClient(APIClient):
    class Config:
        base_url = settings.STARLING_BANK_BASE_URL

    get_account = GetEndpoint(
        '/accounts'
    )

    get_account_balance = GetEndpoint(
        '/accounts/balance'
    )

    get_transactions = GetEndpoint(
        '/transactions',
        querystring_args=(
            Arg('from'),
            Arg('to')
        )
    )

    get_transaction_detail = GetEndpoint(
        '/transactions/{transaction_id}'
    )

    get_mastercard_transactions = GetEndpoint(
        '/transactions/mastercard',
        querystring_args=(
            Arg('from'),
            Arg('to')
        )
    )

    get_mastercard_transaction = GetEndpoint(
        '/transactions/mastercard/{transaction_id}'
    )

    get_direct_debit_transactions = GetEndpoint(
        '/transactions/direct-debit',
        querystring_args=(
            Arg('from'),
            Arg('to')
        )
    )

    get_direct_debit_transaction = GetEndpoint(
        '/transactions/direct-debit/{transaction_id}'
    )

    get_faster_payment_inbound_transactions = GetEndpoint(
        '/transactions/fps/in',
        querystring_args=(
            Arg('from'),
            Arg('to')
        )
    )

    get_faster_payment_inbound_transaction = GetEndpoint(
        '/transactions/fps/in/{transaction_id}'
    )

    get_faster_payment_outbound_transactions = GetEndpoint(
        '/transactions/fps/out',
        querystring_args=(
            Arg('from'),
            Arg('to')
        )
    )

    get_faster_payment_outbound_transaction = GetEndpoint(
        '/transactions/fps/out/{transaction_id}'
    )
