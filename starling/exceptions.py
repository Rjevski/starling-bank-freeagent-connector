class StarlingException(Exception):
    """Base exception for all things Starling."""


class WebhookException(StarlingException):
    """Base exception class for all webhook exceptions."""


class DuplicateWebhookException(WebhookException):
    """A webhook with this ID has already been processed."""


class IncorrectSignatureException(WebhookException):
    """Signature is unknown or invalid."""
