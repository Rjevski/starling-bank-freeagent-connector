import json
from base64 import b64encode
from hashlib import sha512
from unittest import mock

from django.test import TestCase

from starling.models import Account, WebhookEvent


# Create your tests here.


class WebhookEventCreationTestCase(TestCase):
    secret = '4512eb6c-e4c8-4da3-9731-03778464ab58'
    body = """{"webhookNotificationUid":"4fb3c943-8ac8-4630-bc32-2efbc2288678","timestamp":"2018-09-09T15:35:07.553Z","content":{"class":".FasterPaymentInEvent","transactionUid":"4fb3c943-8ac8-4630-bc32-2efbc2288678","amount":1.23,"sourceCurrency":"GBP","sourceAmount":1.23,"counterParty":"Andre Borie","reference":"Hello world again!","type":"TRANSACTION_FASTER_PAYMENT_IN","forCustomer":"You received £1.23 from Andre Borie (Hello world again!)"},"accountHolderUid":"93a0e690-222b-4830-b4a4-24dd5be07eba","webhookType":"TRANSACTION_FASTER_PAYMENT_IN","customerUid":"93a0e690-222b-4830-b4a4-24dd5be07eba","uid":"4fb3c943-8ac8-4630-bc32-2efbc2288678"}""".encode(
        'utf-8')

    def setUp(self):
        self.account = Account.objects.create(
            name='Test account',
            access_token='irrelevant',
            webhook_secret=self.secret
        )

    def test_webhook_event_creation(self):
        signature = b64encode(sha512(self.secret.encode('ascii') + self.body).digest()).decode('ascii')

        with mock.patch('starling.views.handle_webhook_event') as mock_handle_webhook_event:
            resp = self.client.post('/starling/handler', data=self.body, content_type='application/json',
                                    HTTP_X_HOOK_SIGNATURE=signature, secure=True)

        self.assertEqual(resp.status_code, 202)

        event = WebhookEvent.objects.first()
        self.assertEqual(event.payload, json.loads(self.body.decode('utf-8')))
        mock_handle_webhook_event.assert_called_once_with(event)

    def test_webhook_event_creation_duplicate(self):
        signature = b64encode(sha512(self.secret.encode('ascii') + self.body).digest()).decode('ascii')

        with mock.patch('starling.views.handle_webhook_event'):
            resp = self.client.post('/starling/handler', data=self.body, content_type='application/json',
                                    HTTP_X_HOOK_SIGNATURE=signature, secure=True)

        self.assertEqual(resp.status_code, 202)

        with mock.patch('starling.views.handle_webhook_event') as mock_handle_webhook_event:
            resp = self.client.post('/starling/handler', data=self.body, content_type='application/json',
                                    HTTP_X_HOOK_SIGNATURE=signature, secure=True)

        self.assertEqual(resp.status_code, 409)
        self.assertEqual(WebhookEvent.objects.count(), 1)
        mock_handle_webhook_event.assert_not_called()

    def test_webhook_creation_invalid_signature(self):
        resp = self.client.post('/starling/handler', data=self.body, content_type='application/json',
                                HTTP_X_HOOK_SIGNATURE='bad', secure=True)

        self.assertEqual(resp.status_code, 403)
        self.assertEqual(WebhookEvent.objects.count(), 0)
