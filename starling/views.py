from django.db.transaction import atomic
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from starling.exceptions import DuplicateWebhookException, IncorrectSignatureException
from starling.models import WebhookEvent
from starling.services import handle_webhook_event


# Create your views here.

@csrf_exempt
@atomic
def handle_webhook(request):
    try:
        event = WebhookEvent.objects.create_from_request(request)
        handle_webhook_event(event)
        return HttpResponse('', status=202)
    except DuplicateWebhookException:
        return HttpResponse(b'Webhook already processed.', status=409, content_type='text/plain')
    except IncorrectSignatureException:
        return HttpResponse(b'Incorrect/unknown/missing signature.', status=403, content_type='text/plain')
