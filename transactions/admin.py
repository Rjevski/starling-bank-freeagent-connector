from datetime import date

from django.conf import settings
from django.contrib import admin
from django.utils.translation import ngettext

from transactions.models import Link
from transactions.services import sync_link


# Register your models here.

class LinkAdmin(admin.ModelAdmin):
    list_display = ['id', 'starling_account', 'freeagent_account', 'freeagent_instance']
    actions = ['sync']

    def sync(self, request, queryset):
        links = queryset.all()

        for link in links:
            sync_link(link, date.today() - settings.SYNC_PERIOD, date.today())

        message = ngettext(
            '{count} {name} was synced.',
            '{count} {name} were synced.',
            len(links)
        ).format(
            count=len(links),
            name='link' if len(links) == 1 else 'links'
        )

        self.message_user(request, message)


admin.site.register(Link, LinkAdmin)
