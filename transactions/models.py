from django.db import models


# Create your models here.


class Link(models.Model):
    starling_account = models.ForeignKey('starling.Account', on_delete=models.CASCADE, related_name='links')
    freeagent_account = models.ForeignKey('freeagent.BankAccount', verbose_name='FreeAgent account',
                                          on_delete=models.CASCADE, related_name='links')

    @property
    def freeagent_instance(self):
        return self.freeagent_account.instance
