# Generated by Django 2.1.1 on 2018-12-22 18:16

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('transactions', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='freeagent_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='links',
                                    to='freeagent.BankAccount', verbose_name='FreeAgent account'),
        ),
    ]
