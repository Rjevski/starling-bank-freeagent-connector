from django.template.loader import render_to_string

from freeagent.services import upload_statement
from ofx.services import ofx_context
from starling.services import get_transactions
from transactions.models import Link


def sync_link(link: Link, from_, to):
    pending, settled = get_transactions(link.starling_account,
                                        from_, to)
    context = ofx_context(link.starling_account, pending, settled)
    statement = render_to_string('ofx/response.xml', context)

    upload_statement(link.freeagent_account, statement)
