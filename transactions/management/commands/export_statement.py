import argparse
from datetime import date, datetime, timedelta

from django.core.management import BaseCommand
from django.template.loader import render_to_string

from ofx.services import ofx_context
from starling.models import Account
from starling.services import get_transactions


class Command(BaseCommand):
    help = 'Export statement in OFX format.'

    def add_arguments(self, parser):
        parser.add_argument('account', nargs='?', default=Account.objects.first(),
                            type=lambda s: Account.objects.get(pk=s), help='local account ID')
        parser.add_argument('destination', nargs='?', type=argparse.FileType('w'),
                            default='statement.ofx', help='output file')
        parser.add_argument('--from', nargs='?', type=lambda s: datetime.strptime(s, '%Y-%m-%d').date(),
                            default=date.today() - timedelta(days=30),
                            help='from which date?')
        parser.add_argument('--to', nargs='?', type=lambda s: datetime.strptime(s, '%Y-%m-%d').date(),
                            default=date.today(),
                            help='up to which date?')

    def handle(self, *args, **options):
        output = options['destination']
        account = options['account']
        pending, settled = get_transactions(account, options['from'], options['to'])
        context = ofx_context(account, pending, settled)
        output.write(render_to_string('ofx/response.xml', context))
