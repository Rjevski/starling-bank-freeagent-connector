from datetime import date, datetime

from django.conf import settings
from django.core.management import BaseCommand

from transactions.models import Link
from transactions.services import sync_link


class Command(BaseCommand):
    help = 'Uploads transactions to FreeAgent.'

    def add_arguments(self, parser):
        parser.add_argument('from', nargs='?', type=lambda s: datetime.strptime(s, '%Y-%m-%d').date(),
                            default=date.today() - settings.SYNC_PERIOD,
                            help='from which date?')
        parser.add_argument('to', nargs='?', type=lambda s: datetime.strptime(s, '%Y-%m-%d').date(),
                            default=date.today(),
                            help='up to which date?')

    def handle(self, *args, **options):
        for link in Link.objects.all():
            sync_link(link, options['from'], options['to'])
