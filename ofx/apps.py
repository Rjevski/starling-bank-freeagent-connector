from django.apps import AppConfig


class OfxConfig(AppConfig):
    name = 'ofx'
    verbose_name = 'OFX'
