from datetime import datetime

from django.test import TestCase

from ofx.templatetags.ofx import ofx_date, ofx_transaction_type


class DateTimeFilterTestCase(TestCase):
    def test_datetime(self):
        time = datetime(2018, 7, 29, 21, 26, 48, 983312)
        self.assertEqual(ofx_date(time), '20180729212648.983')


class TransactionTypeFilterTestCase(TestCase):
    def test_transaction_type(self):
        self.assertEqual(ofx_transaction_type('DIRECT_DEBIT'), 'DIRECTDEBIT')

    def test_transaction_type_unknown(self):
        self.assertEqual(ofx_transaction_type('UNKNOWN'), 'OTHER')
