from datetime import datetime

from django import template
from django.utils.dateparse import parse_datetime

DATETIME_FORMAT_STRING = '%Y%m%d%H%M%S'

TRANSACTION_TYPE_MAP = {
    'DIRECT_CREDIT': 'DIRECTDEP',
    'DIRECT_DEBIT': 'DIRECTDEBIT',
    'DIRECT_DEBIT_DISPUTE': 'DIRECTDEBIT',
    'INTERNAL_TRANSFER': 'XFER',
    'MASTER_CARD': 'POS',
    'FASTER_PAYMENTS_IN': 'PAYMENT',
    'FASTER_PAYMENTS_OUT': 'PAYMENT',
    'FASTER_PAYMENTS_REVERSAL': 'PAYMENT',
    'STRIPE_FUNDING': 'DEP',
    'INTEREST_PAYMENT': 'INT',
    'NOSTRO_DEPOSIT': 'CHECK',
    'OVERDRAFT': 'INT'
}

register = template.Library()


@register.filter
def iso8601_to_datetime(value: str):
    return parse_datetime(value)


@register.filter
def ofx_date(value: datetime):
    return f'{value.strftime(DATETIME_FORMAT_STRING)}.{value.microsecond // 1000}'


@register.filter
def ofx_transaction_type(value: dict):
    return TRANSACTION_TYPE_MAP.get(value, 'OTHER')
