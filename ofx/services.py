from django.template.loader import render_to_string
from django.utils import timezone

from starling.clients import StarlingClient
from starling.models import Account


def render_statement(account: Account, pending_transactions: list, settled_transactions: list):
    context = ofx_context(account, pending_transactions, settled_transactions)
    return render_to_string('ofx/response.xml', context)


def ofx_context(account: Account, pending_transactions: list, settled_transactions: list):
    account_response = StarlingClient.get_account(_session=account.session)
    balances = StarlingClient.get_account_balance(_session=account.session)

    context = {
        'now': timezone.now(),
        'account': account_response,
        'balances': balances,
        'pending_transactions': pending_transactions,
        'settled_transactions': settled_transactions
    }

    if settled_transactions:
        context.update({
            'first_transaction_date': settled_transactions[0]['created'],
            'last_transaction_date': settled_transactions[-1]['created'],
        })

    return context
