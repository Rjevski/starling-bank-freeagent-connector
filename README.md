# Starling Bank FreeAgent connector

This application allows you to sync Starling Bank transactions to FreeAgent. Some transactions (Faster Payments, etc) can be synced in real-time (with webhooks). Transactions that are subject to change (card transactions can be cancelled or their amount changed) are ignored for webhooks and eventually get synced after they are formally settled (so the final amount is known) by a scheduled task.

## Overview

The app listens for webhooks from Starling Bank. It then fetches the transaction referenced in the webhook from the API (the webhook does not contain all the information required), then if the transaction is settled (if it's a Faster Payment, etc), it will generate an [OFX](https://en.wikipedia.org/wiki/Open_Financial_Exchange) file and then upload it to FreeAgent as a bank statement (FreeAgent does not have an API endpoint to insert individual transactions, so this is a workaround).

Alternatively, a management command (`python manage.py upload_statements`) is provided which will take the latest 30 days worth of settled transactions and upload that to FreeAgent. This is supposed to be run as a scheduled task daily and is designed to catch any transactions that have settled, or any transactions for which the webhook failed. De-duplication is handled by FreeAgent itself - in 6 months of running this in production I have not noticed any instances where duplicate transactions managed to get through.

## Configuration variables

The app requires certain values to be set via environment variables.

* FREEAGENT_BASE_URL: base URL for the FreeAgent API. Use `https://api.sandbox.freeagent.com/v2` for sandbox, `https://api.freeagent.com/v2/` for production.
* FREEAGENT_CLIENT_ID: client ID for your FreeAgent oAuth app. Create it on the [FreeAgent developer dashboard](https://dev.freeagent.com/apps).
* FREEAGENT_CLIENT_SECRET: client secret; same as above.
* STARLING_BANK_BASE_URL: base URL for the Starling Bank API. Use `https://api-sandbox.starlingbank.com/api/v1` for development/sandbox, `https://api.starlingbank.com/api/v1/` for production.
* DATABASE_URL: database URL for a PostgreSQL database.

## Installation

This is designed to be installed on Heroku (and I am successfully running this on their free tier for over 6 months). Just deploy via Git, add the scheduled task, configure the environment variables (you'll need to generate tokens for FreeAgent and Starling Bank), create a superuser using `python manage.py createsuperuser`, log in and then add the required models (you'll need to connect your FreeAgent instance, your Starling Bank account, and create a Link between your Starling Bank account and FreeAgent instance so the app knows which account to sync with which instance).

## No warranty

This code is provided as-is with no guarantee of working. I unfortunately have very little time to polish this up (thus the really awful installation instructions above). Contributions are welcome if someone wants to make this better and more user-friendly.